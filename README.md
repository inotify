## About

A binding to the Linux inotify API for watching file events.

## Examples

Run `chicken-install -n` to install their dependencies and build them.

## Docs

See [its wiki page].

[its wiki page]: http://wiki.call-cc.org/eggref/5/inotify
